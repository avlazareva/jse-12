package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.api.repository.IProjectRepository;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}